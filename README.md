```
vagrant ssh
cd /vagrant/
python -m venv ~/env
source ~/env/bin/activate
deactivate

source ~/env/bin/activate
pip install -r requirements.txt

django-admin.py startproject profiles_project .

python manage.py startapp profiles_api
```

#### edit settings.py

```
<!-- add last 3 apps -->

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'profiles_api',
]


ALLOWED_HOSTS = ['192.168.29.173', 'localhost']
```

### Run 
```
(env) vagrant@ubuntu-bionic:/vagrant$ python manage.py runserver 0.0.0.0:8000
```

### migrate

```
python manage.py makemigrations profiles_api
python manage.py migrate
```

```
(env) vagrant@ubuntu-bionic:/vagrant$ python manage.py createsuperuser
Email: skoal2099@gmail.com
Name: skoal2099
Password:
Password (again):
This password is too common.
Bypass password validation and create user anyway? [y/N]: y
Superuser created successfully.
```


### Deploy
```
curl -sL https://bitbucket.org/waynelei/profiles-rest-api/raw/f00ff028149f9862dc7a8c4f1bd97855948cfee3/deploy/setup.sh | sudo bash -


```